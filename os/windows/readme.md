# _c_ygwin-lite

install git and include git bash. you get most of the core unix tools: grep, cut, find, sort, unix2dos and of course a bash shell.

## unix2dos

```
find . -type f -exec unix2dos {} \;
```

# Extracting files from an MSI 

with a detailed log file 

```msiexec /a "X:\PATH\TO\FILE.MSI" /qb /L*v "Q:\PATH\TO\FILE.LOG" TARGETDIR="Y:\PATH\TO\EXTRACT\FILES\INTO"```



Caveat: you won't know what the script does post file extraction.

tags: msi , msiexec
# _s_hell to file explorer and back again

## dos to file explorer (retains path)

```c:\foo\bar\>explorer .```

## file explorer

in address bar overwrite value with

```cmd .```

## for loopisms

### failing silently (caution)
When you have to ensure dir doesn't fail i.e. oracle external table preprocessor script.  force all output (standard out and error) to standard out and then filter out ```File Not Found```. If no files are found the loop will fall through silently.

```batch
for /f "tokens=*" %%A in ('dir *.zip /b 2^>^&1 ^| FINDSTR /V "File Not Found"') do (
  UNZIP -o %DBDUMP_PATH%\%%A -d D:\APPS\UTIL\FOO
)
```

- bonus tip: you have to redirect standard error to standard out to be able to filter/pipe to another command.
- bonus tip: note the escaped `>, &, |` symbols in the loop expression.
- bonus tip: silence is golden, if you need to kill all output from standard out and error: ```>NUL 2>&1```

see also: http://shaunedonohue.blogspot.co.uk/2007/09/every-time-i-need-to-redirect-dos.html

tags: for , dos , loop , stderr , stdout , redirect

### testes, testes, 1, 2, 3?

```batch
for %i in (1,2,3) do @echo %i
```

```
1
2
3
```
