#vim

##todo

- install fugitive
- git init gitignore template for go?
- swap brackets {} to ()
- rename a function (is there vim-go func? install GoRename)
- use nerdtree or windows to refactor code (is there vim-go func?)
- how to nav between windows and tabs
- how to nav through help: ctrl-] to open help, ctrl-o and t to go back and forth
- how do you toggle (i've seen increment value), there should be a way to either invert i.e. 5 => -5 or true => ffalse

##nerdtree

###to add a new file

```
m, # modify current node
a  # add child node
```

###resources

- http://shebangme.blogspot.co.uk/2010/07/question-is-there-easy-way-to-add-file.html
-
##vim-g

###text objects 

```
<visual>af - to select a function
<visual>if - to select an if block
```
